//dotenv este o librarie utilizata in protejarea datelor gen username pass sau APIKEY 
//exemplu de apelare process.env.+atribut
require('dotenv').config()

const express = require('express');
const app = express();
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
const path = require('path');
const Sequelize = require('sequelize');
const sql=require('sqlite3');

require('dotenv').config()
const port = process.env.PORT ||3006;


//accesul al fisierele downloadate de pe server prin expresss.static
app.use('/', express.static('../my_simple_db_app_front_end/dist'))
app.listen(port,function(){
  console.log('Started on PORT '+port)
})

//accesul al server bidirectional
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//handdler pentru HTTP POST request
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//conectarea la DB locala necesita 3 param 
// const sequelize=new Sequelize('database','username','password',{
//     dialect:'sqlite',
//     host:'localhost',
//     storage:'database.sqlite'
// })
//pentru SQLite se poate seta mai rapid prin:
const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: 'database.sqlite'
});

//testarea conexiunii la DB
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

const Users = sequelize.define('users', {
  idUser: {
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
    type: Sequelize.INTEGER(11)
  },
  username: {
    type: Sequelize.STRING,
    unique: true
  },
  password: {
    type: Sequelize.STRING,
  
  },
});

const Movies = sequelize.define('movies', {
  idMovie: {
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
    type: Sequelize.INTEGER(11)
  },
  movieKey: {
    type: Sequelize.STRING,
  },
  movieName: {
    type: Sequelize.STRING,
  },
  owner: {
    type: Sequelize.STRING
  }
});


const Actors = sequelize.define('actors', {
  idActor: {
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
    type: Sequelize.INTEGER(11)
  },
  actorKey: {
    type: Sequelize.STRING,
  },
  actorName: {
    type: Sequelize.STRING,
  },
  owner: {
    type: Sequelize.STRING
  }
});

Users.hasMany(Movies,{foreignKey:'owner', sourceKey:'username'});
Users.hasMany(Actors,{foreignKey:'owner', sourceKey:'username'});



//creare sau update de tabele 
app.get('/base', (request, response) => {
  sequelize.sync({ force: true }).then(() => {
    response.status(200).send('tables created in DataBase')
  }).catch((err) => {
    console.log(err)
    response.status(200).send('could not create tables users')
  })
})


//request in baza de date 
app.get('/users', (request, response) => {
  Users.findAll().then((results) => {
      response.status(200).json(results)
  }).catch((err) => {
    console.log(err)
    response.status(200).send('error in get/users')
  })
})

app.get('/actors', (request, response) => {
  Actors.findAll().then((results) => {
      response.status(200).json(results)
  }).catch((err) => {
    console.log(err)
    response.status(200).send('error in get/actors')
  })
})

app.get('/movies', (request, response) => {
  Movies.findAll().then((results) => {
      response.status(200).json(results)
  }).catch((err) => {
    console.log(err)
    response.status(200).send('error in get/movies')
  })
})

app.delete('/users/:id', (request, response) => {
  Users.findOne({ where: {idUser: request.params.id}}).then((message) => {
      if(message) {
          message.destroy().then((result) => {
              response.status(204).send('user deleted')
              console.log('record deleted')
          }).catch((err) => {
              console.log(err)
              response.status(500).send('500 Internal Server Error')
          })
      } else {
          response.status(404).send('resource not found')
      }
  }).catch((err) => {
      console.log(err)
      response.status(500).send('500 Internal Server Error')
  })
})

// app.delete('/movies/:id', (request, response) => {
//   Movies.findOne({ where: {idMovie: request.params.id}}).then((message) => {
//       if(message) {
//           message.destroy().then((result) => {
//               response.status(204).send('movie deleted')
//               console.log('record deleted')
//           }).catch((err) => {
//               console.log(err)
//               response.status(500).send('500 Internal Server Error')
//           })
//       } else {
//           response.status(404).send('resource not found')
//       }
//   }).catch((err) => {
//       console.log(err)
//       response.status(500).send('500 Internal Server Error')
//   })
// })

app.delete('/actors/:id', (request, response) => {
  Actors.findOne({ where: {idActor: request.params.id}}).then((message) => {
      if(message) {
          message.destroy().then((result) => {
              response.status(204).send('movie deleted')
              console.log('record deleted')
          }).catch((err) => {
              console.log(err)
              response.status(500).send('500 Internal Server Error')
          })
      } else {
          response.status(404).send('resource not found')
      }
  }).catch((err) => {
      console.log(err)
      response.status(500).send('500 Internal Server Error')
  })
})

app.use(express.json({
  type: ['application/json', 'text/plain']
}));

var request = require('request');
app.use(express.json())
app.use(bodyParser.urlencoded({
  extended: true
}));


app.post('/newmovie', (request, response) => {
  console.log(request)
  Movies.create(request.body).then((result) => {
      response.status(201).json(result)
  }).catch((err) => {
      response.status(500).send("resource not created"+err)
  })
})

app.post('/newactor', (request, response) => {
  console.log(request)
  Actors.create(request.body).then((result) => {
      response.status(201).json(result)
  }).catch((err) => {
      response.status(500).send("resource not created \n")
  })
})


////noul server
//create user
app.post('/registrations', (request, response) => {
  console.log(response)
  Users.create(request.body).then((result) => {
      response.status(200).json(result)
  }).catch((err) => {
      response.status(500).send("resource not created \n")
  })
})

//get user from DB
app.get('/session/:username', (request, response) => {
  Users.findOne({ where: {username: request.params.username}}).then((results) => {
      if(results) {
          //2xx Success  pentru 200=OK si returneaza entitatea  201=Created 202=Accepted 204=No content (returneaza null)
              response.status(200).json(results)
              console.log(results)
      } else {
          response.status(404).send('resource not found')
      }
  }).catch((err) => {
      console.log(err)
      response.status(500).send('500 Internal Server Error')
  })
})

//insert new movie
app.post('/movie', (request, response) => {
  // console.log(request)
  Movies.create(request.body).then((result) => {
      response.status(201).json(result)
      console.log('Resource sent to Database...')
  }).catch((err) => {
      response.status(500).send("resource not created")
      console.log('Resource is already in Database!')
  })
})

//delete movie
app.delete('/movie/:id', (request, response) => {
  Movies.findOne({ where: {movieKey: request.params.id}}).then((message) => {
      if(message) {
          message.destroy().then((result) => {
              response.status(204).send('movie deleted')
              console.log('record deleted')
          }).catch((err) => {
              console.log(err)
              response.status(500).send('| 500 Internal Server Error'+err)
          })
      } else {
          response.status(404).send('resource not found')
      }
  }).catch((err) => {
      console.log(err)
      response.status(500).send('|| 500 Internal Server Error'+err)
  })
})

//find movies by user
app.get('/movie/:owner', (request, response) => {
  Movies.findAll({ where: {owner: request.params.owner}}).then((results) => {
      if(results) {
          //2xx Success  pentru 200=OK si returneaza entitatea  201=Created 202=Accepted 204=No content (returneaza null)
              response.status(200).json(results)
              console.log(results)
      } else {
          response.status(404).send('resource not found')
      }
  }).catch((err) => {
      console.log(err)
      response.status(500).send('500 Internal Server Error')
  })
})
