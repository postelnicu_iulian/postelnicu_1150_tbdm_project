import React, { Component } from 'react';
import Registration from './auth/Registration';
import Login from './auth/Login';
import Users from "./Users.js"
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Link, NavLink } from 'react-router-dom';


export default class home extends Component {
    constructor(props) {
        super(props);

        this.handleSuccessfulAuth = this.handleSuccessfulAuth.bind(this)
    }

    handleSuccessfulAuth(data) {
        this.props.handleLogin(data);
        this.props.history.push("/dashboard"); //redirectionare prin intermediul Route
    }

    render() {
        return (
            <div>
                <h2>Home</h2>
                <h2>Status: {this.props.loggedInStatus}</h2>
                <Registration handleSuccessfulAuth={this.handleSuccessfulAuth} />
                <Login handleSuccessfulAuth={this.handleSuccessfulAuth} />
                <BrowserRouter>
                    <Route
                        exact
                        path={"/users"}
                        render={() => (
                            <Users />
                        )}
                    />
                    <button className="usersList">
                        <NavLink
                            className='navMenuLink'
                            exact to='/users'
                        >Users
                        </NavLink>
                    </button>
                </BrowserRouter>

            </div>
        );
    }
}
