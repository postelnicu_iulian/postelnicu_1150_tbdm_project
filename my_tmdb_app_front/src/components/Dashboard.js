import React from 'react';
import $ from 'jquery';
import MovieRow from "./MovieRow.js";
import Playlist from "./Playlist.js";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import axios from 'axios';
const API_KEY = "c2127638fd40a8d6be837500b0dd5073";

class Dashboard extends React.Component {

  constructor(props) {
    super(props);
    this.state = { }
    this.performSearch()
  }

  performSearch(somethingToSearch) {
    console.log('search for movie online')
    const URLstring = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_KEY + '&query=' + somethingToSearch
    $.ajax({
      url: URLstring,
      success: (searchResults) => {
        const results = searchResults.results
        var movieRows = []

        results.forEach((movie) => {
          movie.owner = this.props.user.username
          movie.poster_source = "https://image.tmdb.org/t/p/w185/" + movie.poster_path
          const movieRow = <MovieRow key={movie.id} movie={movie} />
          movieRows.push(movieRow)
        })
        this.setState({ rows: movieRows })
      },
      error: (xhr, status, err) => {
        console.error('failed to get data')
      }
    })
  }

  //trigger pe search
  searchFormMovie(event) {
    console.log(event.target.value)
    const searchWord = event.target.value
    //bind object face legatura dintre 2 obiecte cel din functie si cel din SearchBar
    const boundObject = this
    boundObject.performSearch(searchWord)
  }

  render() {
    return (
      <div>

        <h2>Dashboard</h2>
        <h2>Status: {this.props.loggedInStatus}</h2>
        <h2>Logged in as: {this.props.user.username}</h2>
        <BrowserRouter>
          <button>
            <NavLink 
            className='navMenuLink' 
            exact to='/playlist'>
              Playlist
              </NavLink>
              </button>
          <Switch>
            <Route 
            path='/playlist' 
            render={() => <Playlist owner={this.props.user.username} />} />
          </Switch>
        </BrowserRouter>
        <input
          className="searchBar"
          onChange={this.searchFormMovie.bind(this)}
          placeholder='Search for movie...'
        />
        {this.state.rows}
      </div>
    )
  }
}

export default Dashboard;