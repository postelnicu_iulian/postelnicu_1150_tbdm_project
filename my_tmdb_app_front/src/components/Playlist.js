import React, { Component } from 'react';
import axios from 'axios';

class Playlist extends Component {

    constructor(props) {
        super(props);
        this.state = {
            movies: []
        }
    }

    //interogarea bazei de date
    // componentDidMount() {

    //     axios.get("http://localhost:3000/movie/" + this.props.owner,
    //         {
    //             'Content-Type': 'application/json',
    //             withCredentials: true
    //         }
    //     )
    //         .then(() => {
    //             console.log("movies received.....");
    //         })
    //         .catch(error => {
    //             console.log(error);
    //         })
    //         .then(movies => this.setState({ movies }, 
    //             console.log("Movies fetched....",
    //             movies)));
    // }

    componentDidMount() {
        fetch('/movie/'+ this.props.owner)
            .then(res => res.json())
            .then(movies => this.setState({ movies }, console.log("Movies fetched....",
                movies)));
    }


    render() {
        return (
            <div >
                <h2>Here are the titles saved by the user!</h2>
                <h4>You can delete the movie if you want, just search the movie and press "DELETE"</h4>
                <ol className="moviesOwned">
                    {this.state.movies.map(movies =>
                        <li key={movies.idMovie}>
                            Movies:
                            {"  "+movies.movieName}
                        </li>
                    )}
                </ol>
            </div>

        )
    }
}
export default Playlist;