import React, { Component } from 'react';

class Users extends Component {

    constructor() {
        super();
        this.state = {
            users: []
        }
    }

    //interogarea bazei de date
    componentDidMount() {
        fetch('/users')
            .then(res => res.json())
            .then(users => this.setState({ users }, console.log("Users fetched....",
                users)));
    }

    render() {
        return (
            <div >
                <h2>Project made with React.js and Node.js</h2>
                <h2>Here are the users from our Database!</h2>
                <ol className="userList">
                    {this.state.users.map(users =>
                        <li key={users.idUser}>
                            Username: 
                            {" "+ users.username}
                        </li>
                    )}
                </ol>
            </div>

        )
    }
}
export default Users;