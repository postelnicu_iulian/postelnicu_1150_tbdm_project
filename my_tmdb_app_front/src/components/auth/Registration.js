import React, { Component } from "react";
import axios from 'axios';

export default class Registration extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: ""
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value //maparea pereche cheie-valoare
        })
    }

    handleSubmit(event) {
        let data = {
            username: this.state.username,
            password: this.state.password
        }

        console.log(this.state)
        axios.post("http://localhost:3000/registrations",
            data,
            {
                'Content-Type': 'application/json',
                withCredentials: true
            }
        )
            .then(response => {
                if (response.data.username !=null) {
                    this.props.handleSuccessfulAuth(response.data);
                }
            })
            .catch(error => {
                console.log(error);
            })


        event.preventDefault()
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <input
                        type="text"
                        name="username"
                        placeholder="Username"
                        value={this.state.username}
                        onChange={this.handleChange}
                        required />

                    <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.handleChange}
                        required />

                    <button type="submit">Register</button>
                </form>
            </div>
        )
    }
}