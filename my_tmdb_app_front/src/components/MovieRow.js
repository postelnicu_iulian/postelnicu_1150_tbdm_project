import React from 'react'
import axios from 'axios';

//pentru a pastra App.js cat mai simpla se creaza scripturi separate ce sunt importate apoi in App.js
//similar cu Fragment din Android
class MovieRow extends React.Component {


  saveTheMovie() {
    let data = {
      movieKey: this.props.movie.id,
      movieName: this.props.movie.title,
      owner: this.props.movie.owner
    }
    axios.post("http://localhost:3000/movie",
      data,
      {
        'Content-Type': 'application/json',
        withCredentials: true
      }
    )
      .then(response => {
        console.log("movie stored.....");
      })
      .catch(error => {
        console.log(error);
      })
  }

  deleteTheMovie() {
    axios.delete("http://localhost:3000/movie/" + this.props.movie.id,
      {
        'Content-Type': 'application/json',
        withCredentials: true
      }
    )
      .then(response => {
        console.log("deleted.....");
      })
      .catch(error => {
        console.log(error);
      })
  }

  render() {
    return <div className="movieRow">
      <table key={this.props.movie.id}>
        <tbody>
          <tr>
            <td>
              <img className='movieImage' alt="img not loaded!" src={this.props.movie.poster_source} />
            </td>
            <td>
              {/* se pot introduce cheile dorite din fisierul JSON returnat !!!!am adaugat aici */}
              <h2>{this.props.movie.title}</h2>
              <p>{this.props.movie.overview}</p>
              <p>{this.props.movie.popularity + ' Popularity'}</p>
              <p>{this.props.movie.vote_count + ' Like'}</p>
              <div>
                <button
                  type='button'
                  onClick={this.saveTheMovie.bind(this)}
                >Save</button>
                <button
                  type='button'
                  onClick={this.deleteTheMovie.bind(this)}
                >Delete</button>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  }
}

export default MovieRow