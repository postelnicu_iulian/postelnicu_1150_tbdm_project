import React, { Component } from 'react';
import Home from './components/Home';
import Users from "./components/Users.js"
import './App.css';
import logo from './tmdbicon.png';
import Dashboard from './components/Dashboard.js';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { NavLink } from 'react-router-dom';


export default class App extends Component {
  constructor() {
    super();

    this.state = {
      loggedInStatus: "NOT_LOGGED_IN",
      user: {}
    };

    this.handleLogin = this.handleLogin.bind(this)
  }

  //modificareStare
  componentDidUpdate() {
    window.localStorage.setItem('state', JSON.stringify(this.state));
  }
  componentDidMount() {
    try {
      const state = window.localStorage.getItem('state');
      this.setState({ ...JSON.parse(state) });
    } catch (e) { }
  }


  //la refresh pierde starea npm install redux-persist 
  // checkLoginStatus() {
  //   axios.get('http://localhost:3000/+API endpoint', { withCredentials: true }).then(response => {
  //     console.log("logged in ?")
  //   }).catch(error => {
  //     console.log("check login error", error)
  //   })
  // }
  // //apelarea functiei de fiecare data cand App este rulata
  // componentDidMount() {
  //   this.checkLoginStatus()
  // }

  handleLogin(data) {
    this.setState({
      loggedInStatus: "LOGGED_IN",
      user: data
    })
  } //face update la stare -modifica Log_in_Status si atribuie valori catre USER

  render() {
    return (
      <div className="App">
        <table className="tittleBar">
          <tbody>
            <tr>
              <td>
                <img  width='50' src={logo} />
              </td>
              <td>
                <h3>MovieDB Search</h3>
              </td>
            </tr>
          </tbody>
        </table>



        <BrowserRouter>
          <Switch>
            <Route
              exact
              path={"/"}
              render={props => (
                <Home
                  {...props}
                  handleLogin={this.handleLogin}
                  loggedInStatus={this.state.loggedInStatus} />
              )}
            />
            <Route
              exact
              path={"/dashboard"}
              render={props => (
                <Dashboard
                  {...props}
                  loggedInStatus={this.state.loggedInStatus}
                  user={this.state.user} />
              )}
            />
            <Route
              exact
              path={"/users"}
              render={() => (
                <Users />
              )}
            />
            <button className="btnUsers">
              <NavLink
                className='navMenuLink'
                exact to='/users'
              >Users
              </NavLink>
            </button>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

